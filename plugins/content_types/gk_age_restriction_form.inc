<?php

$plugin = array(
  'title' => 'Age Restriction: Form',
  'category' => 'GK Age Restriction',
  'single' => TRUE,
);

function gk_age_restriction_gk_age_restriction_form_content_type_render($subtype, $conf, $args, $context) {
  return (object) array(
    'title' => $conf['override_title'] ? $conf['override_title_text'] : '',
    'content' => drupal_get_form('gk_age_restriction_form'),
  );
}

function gk_age_restriction_gk_age_restriction_form_content_type_edit_form($form, &$form_state) {
  return $form;
}

function gk_age_restriction_form() {
  $form['date_of_birth'] = array(
    '#type' => 'date',
    '#title' => 'Date of birth',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function gk_age_restriction_form_submit($form, &$form_state) {
  $required_age = variable_get('gk_age_restriction_required_age', 18);

  $date_of_birth = $form_state['values']['date_of_birth'];
  $date_of_birth = mktime(0, 0, 0, $date_of_birth['month'], $date_of_birth['day'], $date_of_birth['year']);

  if ($date_of_birth > strtotime("$required_age years ago")) {
    if ($denied_message = variable_get('gk_age_restriction_denied_message')) {
      drupal_set_message(t($denied_message));
    }
  }
  else {
    $form_state['redirect'] = '<front>';
    user_cookie_save(array('age_restriction' => 1));
  }
}
