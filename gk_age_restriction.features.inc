<?php
/**
 * @file
 * gk_age_restriction.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gk_age_restriction_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
}
