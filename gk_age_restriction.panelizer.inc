<?php
/**
 * @file
 * gk_age_restriction.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function gk_age_restriction_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:gk_age_restriction';
  $panelizer->title = 'Age Restriction';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '497660e9-746a-4d6f-8e4a-192924fe4e0f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-6ecbd932-6ff2-4830-9c62-1bcf55f409df';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6ecbd932-6ff2-4830-9c62-1bcf55f409df';
    $display->content['new-6ecbd932-6ff2-4830-9c62-1bcf55f409df'] = $pane;
    $display->panels['primary'][0] = 'new-6ecbd932-6ff2-4830-9c62-1bcf55f409df';
    $pane = new stdClass();
    $pane->pid = 'new-16e3a696-de0e-46ae-b1d5-6d98cdb95602';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '16e3a696-de0e-46ae-b1d5-6d98cdb95602';
    $display->content['new-16e3a696-de0e-46ae-b1d5-6d98cdb95602'] = $pane;
    $display->panels['primary'][1] = 'new-16e3a696-de0e-46ae-b1d5-6d98cdb95602';
    $pane = new stdClass();
    $pane->pid = 'new-69038b10-4dde-42c6-8150-0c34c587d535';
    $pane->panel = 'primary';
    $pane->type = 'gk_age_restriction_form';
    $pane->subtype = 'gk_age_restriction_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '69038b10-4dde-42c6-8150-0c34c587d535';
    $display->content['new-69038b10-4dde-42c6-8150-0c34c587d535'] = $pane;
    $display->panels['primary'][2] = 'new-69038b10-4dde-42c6-8150-0c34c587d535';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:gk_age_restriction'] = $panelizer;

  return $export;
}
